#!/usr/bin/env python
# dummy_qual_from_fasta.py --- 
# 
# Filename: dummy_qual_from_fasta.py
# Description: 
# Author: Michele
# Maintainer: 
# Created: Wed Aug 31 13:30:37 2011 (+0200)
# Version: 
# Last-Updated: 
#           By: 
#     Update #: 0
# URL: 
# Keywords: 
# Compatibility: 
# 
# 

# Commentary: 
# You coul add generation of an error profile
# 
# 
# 

# Change Log:
# 
# 
# 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA.
# 
# 

# Code:


# argument parser
import argparse
# system-specific parameters
# and function
import sys
# implements pseudo-random number
# generators for various distributions.
import random
# utilities of various kinds for
# the construction of biological pypelines
from PipeUtils import PipeUtil
# biopython sequence Input/Output module
from Bio import SeqIO



def main(arguments):

    sys.argv = arguments
    # parse command line arguments
    args = arg_parse()

    min_start = int(0)
    max_end = int(50)

    if args.start_value < min_start:
        sys.exit("Start value below %i! Stop!" % min_start)
    if args.end_value > max_end:
        sys.exit("End value higher then %i! Stop!" % max_end)
    

    # new PipeUtil obj
    util = PipeUtil()
    # get python dict of fasta sequences
    fasta_dict = util.get_fasta_index(args.fasta_file)

    seq_records = []

    for ID in fasta_dict.keys():
        phred_scores = []
        seq_record = fasta_dict[ID]

        # chooses a int random number between start and end.
        # The maximum allowed is 50 (maximum Phred quality)
        for base in seq_record:
            phred_scores.append(random.randint(args.start_value, args.end_value))
        # add qualities to SeqRecords obj
        seq_record.letter_annotations["phred_quality"] = phred_scores
        seq_records.append(seq_record)

    out_file_handle = open(args.out_file, 'w')
    SeqIO.write(seq_records, out_file_handle, "qual")
    out_file_handle.close()
        
    return 0






def arg_parse():
    """
    Parses command line arguments
    """
    parser = argparse.ArgumentParser(description="Given a fasta file makes a file in Phred quality format, giving each base a random value between [--start] and [--end].")
    parser.add_argument("--fasta", "-f", dest="fasta_file", required=True, help="Full path of a valid file in fasta format")
    parser.add_argument("--out", "-o", dest="out_file", default="out.fasta.qual", required=False, help="Name of output file (Phred fasta quality)")
    parser.add_argument("--start", "-s", dest="start_value", default=0, required=False, type=int, help="Start value in the range of Phred quality (0-50)")
    parser.add_argument("--end", "-e", dest="end_value", default=50, required=False, type=int, help="End value in the range of Phred quality (0-50)")


    args = parser.parse_args()
    
    return args




# Is invoked when the module is executed
if __name__ == "__main__":
    # Explicitly pass command line arguments
    # because required vor invoke main from
    # other script
    main(sys.argv)


# 
# dummy_qual_from_fasta.py ends here
