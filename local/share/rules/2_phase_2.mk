### phase_2.mk --- 
## 
## Filename: phase_2.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Thu Sep  1 09:59:38 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

# sam alignment
extern ../phase_1/radtags_aln.sam as RADTAGS_ALN_SAM


assembly_ln.fasta: $(ASSEMBLY)
	ln -sf $(ASSEMBLY) $@



# index reference sequence
assembly_ln.fasta.fai: assembly_ln.fasta
	samtools faidx $<

# output alignments with MAPQ(uality) >= 1 in bam format
radtags_aln.bam: assembly_ln.fasta.fai $(RADTAGS_ALN_SAM)
	samtools view -uq1 -t $< $^2 -o $@

# sort alignments by leftmost coordinates
radtags_aln_sorted.bam: radtags_aln.bam
	samtools sort -o $< out.prefix > $@

# index sorted alignment for fast random access
radtags_aln_sorted.bai: radtags_aln_sorted.bam
	samtools index $< ;
	mv $(addsuffix .bai,$<) $@

# return a tab delimited file with:
# contig length aligned_reads not_aligned_reads
# filters line with aligned_reads > 0
radtags_aln_tab.txt: radtags_aln_sorted.bam
	samtools idxstats $< | \
	bawk '!/^[$$,\#+]/ { \
	split($$0,a,"\t"); \
	# for (i = 1; i <= NF; i++) \
	#     { \
	# 	printf "%i\t%s\n", i, a[i]; \
	#     } \
	if (a[3] > 0) \
	{ \
	print $$0; \
	} \
	}' > $@

# call SNPs and short INDELs. Save in BCF (binary variant call format)
radtags_var_raw.bcf: assembly_ln.fasta radtags_aln_sorted.bam
	samtools mpileup -uf $< $^2 | bcftools view -bvcg - > $@

# convert BCF to VCF. Sets the maximum read depth to call a SNP to 5 (-D)
radtags_var_flt.vcf: radtags_var_raw.bcf
	bcftools view $< | vcfutils varFilter -D5 > $@

# convert from VCF format to tab-delimited text file 
radtags_var_flt_tab.txt: radtags_var_flt.vcf
	vcf-to-tab < $< > $@




# Standard Phony Targets for Users.
 
# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += assembly_ln.fasta \
	 assembly_ln.fasta.fai \
	 radtags_aln.bam \
	 radtags_aln_sorted.bam \
	 radtags_aln_sorted.bai \
	 radtags_aln_tab.txt \
	 radtags_var_raw.bcf \
	 radtags_var_flt.vcf \
	 radtags_var_flt_tab.txt


# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE +=




# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += assembly_ln.fasta \
	 assembly_ln.fasta.fai \
	 radtags_aln.bam \
	 radtags_aln_sorted.bam \
	 radtags_aln_sorted.bai \
	 radtags_aln_tab.txt \
	 radtags_var_raw.bcf \
	 radtags_var_flt.vcf \
	 radtags_var_flt_tab.txt


# Declare clean as
# phony targhet
.PHONY: clean
# so i can recall function to
# delete dir or other files
clean:


######################################################################
### phase_2.mk ends here
