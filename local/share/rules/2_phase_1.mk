### phase_1.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Tue Aug 30 18:40:57 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

# full marker set from RADtools
RADMARKERS_FILE ?=
# transcriptome assembly
ASSEMBLY ?=
# num threads for aligner
NUM_THREADS ?=


radmarkers_file_ln: $(RADMARKERS_FILE)
	ln -sf $(RADMARKERS_FILE) $@

assembly_ln.fasta: $(ASSEMBLY)
	ln -sf $(ASSEMBLY) $@




# creates a fasta file by removing redundant sequences
radtags.fasta: radmarkers_file_ln
	FASTA_INTERMID=tmp.fasta; \
	bawk '!/^[$$,\#+]/ FNR>1 { \
	# escape first line \
	if (FNR>1) \
	{ \
	split($$0,a,"\t"); \
	# for (i = 1; i <= NF; i++) \
	#     { \
	# 	printf "%i\t%s\n", i, a[i]; \
	#     } \
	printf "%s_%i\t%s\n", a[4], a[1], a[4]; \
	} \
	}' $< | \
	# sort lines \
	bsort | \
	# discard all but one of successive identical lines \
	uniq -c | \
	# remove number of occurrences that prefix lines \
	bawk '/[^[:space:]]/ { \
	split($$0,a," "); \
	printf "%s\t%s\n", a[2], $$2; \
	}' | \
	# make a fasta \
	tab2fasta > $$FASTA_INTERMID; \
	# go to newline in fasta \
	fastatool sanitize $$FASTA_INTERMID $@; \
	rm -f $$FASTA_INTERMID;



# generates a quality
radtags.fasta.qual: radtags.fasta
	dummy_qual_from_fasta --fasta $< --start 40 --out $@


# transform to fstq
radtags.fastq: radtags.fasta radtags.fasta.qual
	qualfa2fq $< $^2 > $@



# indexes the referece
# add -n
assembly_bwa_db: assembly_ln.fasta
	bwa index -p $@ -a bwtsw $<


# align on reference
radtags_aln.sai: assembly_bwa_db radtags.fastq
	bwa aln -t $(NUM_THREADS) $< $^2 > $@


# tranform to sam
radtags_aln.sam: assembly_bwa_db radtags_aln.sai radtags.fastq
	bwa samse $< $^2 $^3 > $@



# Standard Phony Targets for Users.
 
# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += radmarkers_file_ln \
	 assembly_ln.fasta \
	 radtags.fasta \
	 radtags.fasta.qual \
	 radtags.fastq \
	 assembly_bwa_db \
	 radtags_aln.sai \
	 radtags_aln.sam


# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE +=




# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += radmarkers_file_ln \
	 assembly_ln.fasta \
         radtags.fasta \
	 radtags.fasta.qual \
	 radtags.fastq \
	 radtags_aln.sai \
	 radtags_aln.sam



# Declare clean as
# phony targhet
.PHONY: clean
# so i can recall function to
# delete dir or other files
clean:
	rm assembly_bwa_db.*



######################################################################
### phase_1.mk ends here
